$(document).ready(function(){
    $("#show").click(function(){
        $("#notes").toggle();
        
        if ($.trim($(this).text()) === 'Pokaż wpisy') {
            $(this).text('Ukryj wpisy');
        } else {
            $(this).text('Pokaż wpisy');        
        }
    });
     
    $("#notes").toggle();
});