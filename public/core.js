var Note = angular.module('Note', []);

function mainController($scope, $http) {
    $scope.formData = {};

    $http.get('/api/notes')
        .success(function(data) {
            $scope.notes = data;
            console.log(data);
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });

    $scope.createNote = function() {
        $http.post('/api/notes', $scope.formData).success(function(data) {
                $scope.formData = {}; 
                $scope.notes = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };

    $scope.deleteNote = function(id) {
        $http.delete('/api/notes/' + id).success(function(data) {
                $scope.notes = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };

}