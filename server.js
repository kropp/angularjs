// server.js ----------------------------------------------------------------
    var express  = require('express');
    var app      = express();                              
    var mongoose = require('mongoose');                    
    var morgan = require('morgan');            
    var bodyParser = require('body-parser');    
    var methodOverride = require('method-override'); 

// mongodb connect ----------------------------------------------------------
    mongoose.connect('mongodb://Kropp:123456@proximus.modulusmongo.net:27017/r7uVoxuv');     

    app.use(express.static(__dirname + '/public'));                 
    app.use(morgan('dev'));                                        
    app.use(bodyParser.urlencoded({'extended':'true'}));           
    app.use(bodyParser.json());                                     
    app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 
    app.use(methodOverride());

// listen ------------------------------------------------------------------
    app.listen(8080);
    console.log("Aplikacja działa na porcie 8080");

// model -------------------------------------------------------------------
   
    var Note = mongoose.model('Note', {
        text : String,
        date : String,
        author : String
    });

// api ---------------------------------------------------------------------
    app.get('/api/notes', function(req, res) {

        Note.find(function(err, notes) {
            if (err)
                res.send(err)

            res.json(notes); 
        });
    });

    app.post('/api/notes', function(req, res) {

        Note.create({
            text : req.body.text,
            date : req.body.date,
            author : req.body.author,
            done : false
        }, function(err, note) {
            if (err)
                res.send(err);

            Note.find(function(err, notes) {
                if (err)
                    res.send(err)
                res.json(notes);
            });
        });

    });

    app.delete('/api/notes/:note_id', function(req, res) {
        Note.remove({
            _id : req.params.note_id
        }, function(err, note) {
            if (err)
                res.send(err);

            // get and return all the notes after you create another
            Note.find(function(err, notes) {
                if (err)
                    res.send(err)
                res.json(notes);
            });
        });
    });

 // application -------------------------------------------------------------
    app.get('*', function(req, res) {
        res.sendfile('./public/index.html');
    });
